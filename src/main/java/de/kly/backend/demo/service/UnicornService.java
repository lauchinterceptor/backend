package de.kly.backend.demo.service;

import de.kly.backend.demo.domain.Unicorn;
import de.kly.backend.demo.port.driven.CreateUnicornPort;
import de.kly.backend.demo.port.driven.DeleteUnicornPort;
import de.kly.backend.demo.port.driven.GetUnicornPort;
import de.kly.backend.demo.port.driven.UpdateUnicornPort;
import de.kly.backend.demo.port.driver.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnicornService
        implements CreateUnicornUseCase, GetUnicornUseCase, DeleteUnicornUseCase, UpdateUnicornUseCase {

    @Autowired
    CreateUnicornPort createUnicornPort;
    @Autowired
    GetUnicornPort getUnicornPort;
    @Autowired
    DeleteUnicornPort deleteUnicornPort;
    @Autowired
    UpdateUnicornPort updateUnicornPort;

    @Override
    public long createUnicorn(CreateUnicornCommand command) {
        return createUnicornPort.createUnicorn(command);
    }

    @Override
    public Unicorn getUnicornById(long id) {
        return getUnicornPort.getUnicornById(id);
    }

    @Override
    public List<Unicorn> getAllUnicorns() {
        return getUnicornPort.getAllUnicorns();
    }

    @Override
    public void deleteUnicorn(long id) {
        deleteUnicornPort.deleteUnicorn(id);
    }

    @Override
    public void performExtinction() {
        deleteUnicornPort.performExtinction();
    }

    @Override
    public Unicorn updateUnicorn(UpdateUnicornCommand command, long id) {
        return updateUnicornPort.updateUnicorn(command, id);
    }

}
