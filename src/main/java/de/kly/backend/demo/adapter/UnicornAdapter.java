package de.kly.backend.demo.adapter;

import de.kly.backend.demo.UnicornMapper;
import de.kly.backend.demo.domain.Unicorn;
import de.kly.backend.demo.domain.UnicornEntity;
import de.kly.backend.demo.port.driven.CreateUnicornPort;
import de.kly.backend.demo.port.driven.DeleteUnicornPort;
import de.kly.backend.demo.port.driven.GetUnicornPort;
import de.kly.backend.demo.port.driven.UpdateUnicornPort;
import de.kly.backend.demo.port.driver.CreateUnicornCommand;
import de.kly.backend.demo.port.driver.UpdateUnicornCommand;
import de.kly.backend.demo.repository.UnicornRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UnicornAdapter implements CreateUnicornPort, GetUnicornPort, DeleteUnicornPort, UpdateUnicornPort {

    @Autowired
    UnicornRepository unicornRepository;

    UnicornMapper unicornMapper = new UnicornMapper();

    @Transactional
    public long createUnicorn(CreateUnicornCommand command) {
        UnicornEntity unicornEntity = new UnicornEntity(0, command.getName(), command.getHornLength(),
                command.isCanFly());
        return unicornRepository.save(unicornEntity).getId();
    }

    @Override
    public void deleteUnicorn(long id) {
        unicornRepository.deleteById(id);
    }

    @Override
    public void performExtinction() {
        unicornRepository.deleteAll();
    }

    @Override
    public Unicorn getUnicornById(long id) {
        UnicornEntity unicornEntity = unicornRepository.getById(id);
        return unicornMapper.mapEntity(unicornEntity);
    }

    @Override
    public List<Unicorn> getAllUnicorns() {
        return unicornMapper.mapEntities(unicornRepository.findAll());
    }

    @Override
    public Unicorn updateUnicorn(UpdateUnicornCommand command, long id) {
        UnicornEntity updatedUnicorn = unicornRepository.getById(id);
        updatedUnicorn.setName(command.getName());
        updatedUnicorn.setHornLength(command.getHornLength());
        updatedUnicorn.setCanFly(command.isCanFly());
        return unicornMapper.mapEntity(unicornRepository.save(updatedUnicorn));
    }
}
