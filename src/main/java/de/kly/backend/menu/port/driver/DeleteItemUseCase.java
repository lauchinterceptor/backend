package de.kly.backend.menu.port.driver;

import java.util.List;

public interface DeleteItemUseCase {
    void deleteItem(long id);

    void deleteItems(List<Long> ids);
}
