package de.kly.backend.demo.port.driven;

import de.kly.backend.demo.domain.Unicorn;
import de.kly.backend.demo.port.driver.UpdateUnicornCommand;

public interface UpdateUnicornPort {

    Unicorn updateUnicorn(UpdateUnicornCommand command, long id);

}
