package de.kly.backend.demo.port.driven;

public interface DeleteUnicornPort {

    void deleteUnicorn(long id);

    void performExtinction();

}
