package de.kly.backend.menu.port.driver;

import de.kly.backend.menu.domain.Item;

import java.util.List;

public interface AddItemUseCase {

    Item addItem(Item newItem);

    List<Item> addItems(List<Item> newItems);

}
