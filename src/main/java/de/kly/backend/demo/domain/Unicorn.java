package de.kly.backend.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Unicorn {
    int id;
    String name;
    float hornLength;
    boolean canFly;
}
