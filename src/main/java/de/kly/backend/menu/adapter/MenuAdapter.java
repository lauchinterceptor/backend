package de.kly.backend.menu.adapter;

import de.kly.backend.menu.ItemRepository;
import de.kly.backend.menu.domain.Item;
import de.kly.backend.menu.entity.ItemEntity;
import de.kly.backend.menu.mapper.ItemMapper;
import de.kly.backend.menu.port.driven.AddItemPort;
import de.kly.backend.menu.port.driven.DeleteItemPort;
import de.kly.backend.menu.port.driven.GetItemPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuAdapter implements AddItemPort, GetItemPort, DeleteItemPort {

    ItemRepository itemRepository;

    ItemMapper itemMapper;

    @Autowired
    public MenuAdapter(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
        this.itemMapper = new ItemMapper();
    }

    @Override
    public Item addItem(Item newItem) {
        ItemEntity itemEntity = itemRepository.save(itemMapper.mapDomainObject(newItem));
        return itemMapper.mapEntity(itemEntity);
    }

    @Override
    public List<Item> addItems(List<Item> newItems) {
        List<ItemEntity> itemEntities = itemRepository.saveAll(itemMapper.mapDomainObjects(newItems));
        return itemMapper.mapEntities(itemEntities);
    }

    @Override
    public void deleteItem(long id) {
        itemRepository.deleteById(id);
    }

    @Override
    public void deleteItems(List<Long> ids) {
        itemRepository.deleteAllById(ids);
    }

    @Override
    public Item getItemById(long id) {
        return itemMapper.mapEntity(itemRepository.getById(id));
    }

    @Override
    public List<Item> getItems() {
        return itemMapper.mapEntities(itemRepository.findAll());
    }

    @Override
    public List<Item> getItemsById(List<Long> ids) {
        return itemMapper.mapEntities(itemRepository.findAllById(ids));
    }
}
