package de.kly.backend.demo.port.driven;

import de.kly.backend.demo.domain.Unicorn;

import java.util.List;

public interface GetUnicornPort {

    Unicorn getUnicornById(long id);

    List<Unicorn> getAllUnicorns();

}
