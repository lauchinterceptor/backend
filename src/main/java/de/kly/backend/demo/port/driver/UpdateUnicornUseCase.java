package de.kly.backend.demo.port.driver;

import de.kly.backend.demo.domain.Unicorn;

public interface UpdateUnicornUseCase {

    Unicorn updateUnicorn(UpdateUnicornCommand command, long id);

}
