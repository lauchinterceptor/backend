package de.kly.backend.menu.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Item {
    long id;
    String name;
    String description;
    BigDecimal price;
}
