package de.kly.backend.menu.port.driven;

import java.util.List;

public interface DeleteItemPort {
    void deleteItem(long id);

    void deleteItems(List<Long> ids);
}
