package de.kly.backend.demo.repository;

import de.kly.backend.demo.domain.UnicornEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnicornRepository extends JpaRepository<UnicornEntity, Long> {
}
