package de.kly.backend.demo.port.driver;

import de.kly.backend.demo.domain.Unicorn;

public interface CreateUnicornUseCase {

    long createUnicorn(CreateUnicornCommand command);

}
