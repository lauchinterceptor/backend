package de.kly.backend.menu.port.driven;

import de.kly.backend.menu.domain.Item;

import java.util.List;

public interface AddItemPort {
    Item addItem(Item newItem);

    List<Item> addItems(List<Item> newItems);
}
