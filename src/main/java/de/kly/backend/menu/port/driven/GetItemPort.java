package de.kly.backend.menu.port.driven;

import de.kly.backend.menu.domain.Item;

import java.util.List;

public interface GetItemPort {
    Item getItemById(long id);

    List<Item> getItems();

    List<Item> getItemsById(List<Long> ids);

}
