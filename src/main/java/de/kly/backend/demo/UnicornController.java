package de.kly.backend.demo;

import de.kly.backend.demo.domain.Unicorn;
import de.kly.backend.demo.port.driver.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/unicorn")
public class UnicornController {

    @Autowired
    CreateUnicornUseCase createUnicornUseCase;
    @Autowired
    GetUnicornUseCase getUnicornUseCase;
    @Autowired
    UpdateUnicornUseCase updateUnicornUseCase;
    @Autowired
    DeleteUnicornUseCase deleteUnicornUseCase;

    @PostMapping("")
    ResponseEntity<Long> createUnicorn(CreateUnicornCommand command) {
        return new ResponseEntity<>(createUnicornUseCase.createUnicorn(command), HttpStatus.OK);
    }

    @GetMapping("/{unicornId}")
    ResponseEntity<Unicorn> getUnicorn(@PathVariable("unicornId") long unicornId) {
        return new ResponseEntity<>(getUnicornUseCase.getUnicornById(unicornId), HttpStatus.OK);
    }

    @GetMapping("")
    ResponseEntity<List<Unicorn>> getAllUnicorns() {
        return new ResponseEntity<>(getUnicornUseCase.getAllUnicorns(), HttpStatus.OK);
    }

    @PutMapping("/{unicornId}")
    ResponseEntity<Unicorn> updateUnicorn(UpdateUnicornCommand command, @PathVariable("unicornId") long id) {
        return new ResponseEntity<>(updateUnicornUseCase.updateUnicorn(command, id), HttpStatus.OK);
    }

    @DeleteMapping("/{unicornId}")
    ResponseEntity<HttpStatus> deleteUnicorn(@PathVariable("unicornId") long id) {
        deleteUnicornUseCase.deleteUnicorn(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    ResponseEntity<HttpStatus> performExtinction() {
        deleteUnicornUseCase.performExtinction();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
