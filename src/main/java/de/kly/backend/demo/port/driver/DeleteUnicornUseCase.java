package de.kly.backend.demo.port.driver;

public interface DeleteUnicornUseCase {

    void deleteUnicorn(long id);

    void performExtinction();
}
