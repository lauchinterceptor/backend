package de.kly.backend.demo.port.driver;

import lombok.Data;

@Data
public class UpdateUnicornCommand {
    String name;
    float hornLength;
    boolean canFly;
}
