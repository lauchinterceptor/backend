package de.kly.backend.menu.service;

import de.kly.backend.menu.domain.Item;
import de.kly.backend.menu.port.driven.AddItemPort;
import de.kly.backend.menu.port.driven.DeleteItemPort;
import de.kly.backend.menu.port.driven.GetItemPort;
import de.kly.backend.menu.port.driver.AddItemUseCase;
import de.kly.backend.menu.port.driver.DeleteItemUseCase;
import de.kly.backend.menu.port.driver.GetItemUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService implements AddItemUseCase, GetItemUseCase, DeleteItemUseCase {

    AddItemPort addItemPort;
    GetItemPort getItemPort;
    DeleteItemPort deleteItemPort;

    @Autowired
    public MenuService(AddItemPort addItemPort, GetItemPort getItemPort, DeleteItemPort deleteItemPort) {
        this.addItemPort = addItemPort;
        this.getItemPort = getItemPort;
        this.deleteItemPort = deleteItemPort;
    }

    @Override
    public Item addItem(Item newItem) {
        return addItemPort.addItem(newItem);
    }

    @Override
    public List<Item> addItems(List<Item> newItems) {
        return addItemPort.addItems(newItems);
    }

    @Override
    public Item getItemById(long id) {
        return getItemPort.getItemById(id);
    }

    @Override
    public List<Item> getItems() {
        return getItemPort.getItems();
    }

    @Override
    public List<Item> getItemsById(List<Long> ids) {
        return getItemPort.getItemsById(ids);
    }

    @Override
    public void deleteItem(long id) {
        deleteItemPort.deleteItem(id);
    }

    @Override
    public void deleteItems(List<Long> ids) {
        deleteItemPort.deleteItems(ids);
    }
}
