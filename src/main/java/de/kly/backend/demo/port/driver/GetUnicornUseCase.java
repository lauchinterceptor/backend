package de.kly.backend.demo.port.driver;

import de.kly.backend.demo.domain.Unicorn;

import java.util.List;

public interface GetUnicornUseCase {
    Unicorn getUnicornById(long id);

    List<Unicorn> getAllUnicorns();
}
