package de.kly.backend.demo;

import de.kly.backend.Mapper;
import de.kly.backend.demo.domain.Unicorn;
import de.kly.backend.demo.domain.UnicornEntity;

public class UnicornMapper implements Mapper<Unicorn, UnicornEntity> {

    @Override
    public Unicorn mapEntity(UnicornEntity entity) {
        return new Unicorn(entity.getId(), entity.getName(), entity.getHornLength(), entity.isCanFly());
    }

    public UnicornEntity mapDataObject(Unicorn domainObject) {
        return new UnicornEntity(domainObject.getId(), domainObject.getName(), domainObject.getHornLength(),
                domainObject.isCanFly());
    }

}
