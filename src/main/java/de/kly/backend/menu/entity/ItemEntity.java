package de.kly.backend.menu.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "Item")
@Table(name = "Item")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "name", nullable = false, unique = true)
    String name;
    @Column(name = "description", length = 400)
    String description;
    @Column(name = "price", precision = 8, scale = 2)
    BigDecimal price;
}
