package de.kly.backend.menu.mapper;

import de.kly.backend.Mapper;
import de.kly.backend.menu.domain.Item;
import de.kly.backend.menu.entity.ItemEntity;

public class ItemMapper implements Mapper<Item, ItemEntity> {

    @Override
    public Item mapEntity(ItemEntity entity) {
        return new Item(entity.getId(), entity.getName(), entity.getDescription(), entity.getPrice());
    }

    @Override
    public ItemEntity mapDomainObject(Item domainObject) {
        return new ItemEntity(domainObject.getId(), domainObject.getName(), domainObject.getDescription(),
                domainObject.getPrice());
    }
}
