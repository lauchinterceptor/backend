package de.kly.backend.menu.port.driver;

import de.kly.backend.menu.domain.Item;

import java.util.List;

public interface GetItemUseCase {
    Item getItemById(long id);

    List<Item> getItems();

    List<Item> getItemsById(List<Long> ids);
}
