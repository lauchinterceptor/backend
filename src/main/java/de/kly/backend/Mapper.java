package de.kly.backend;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface Mapper<T, E> {

    T mapEntity(E entity);

    E mapDataObject(T domainObject);

    default List<T> mapEntities(Collection<E> graphObjects) {
        return graphObjects.parallelStream().map(this::mapEntity).collect(Collectors.toList());
    }

    default List<E> mapDataObjects(List<T> domainObjects) {
        return domainObjects.parallelStream().map(this::mapDataObject).collect(Collectors.toList());
    }
}
