package de.kly.backend.demo.port.driven;

import de.kly.backend.demo.port.driver.CreateUnicornCommand;

public interface CreateUnicornPort {

    long createUnicorn(CreateUnicornCommand command);

}
